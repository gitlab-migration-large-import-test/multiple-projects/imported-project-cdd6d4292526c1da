# GitLab QA trainings

The handbook has [information about the entire test automation framework](https://about.gitlab.com/handbook/engineering/quality/#test-automation-framework), including GitLab QA. There you can find links to additional documentation and videos.

## Build team training on GitLab QA

A great introduction to the GitLab QA project, its internal structure, how it's
used, and how to start contributing to it. Check out the
[video](https://youtu.be/Ym159ATYN_g) and
[slides](https://docs.google.com/presentation/d/1-3YlYTIBzd2kSjGVYGPq1xqz4O7qtfABhoZRi_PcGRg/edit?usp=sharing).

## GitLab QA at FOSDEM

[Grzegorz's presentation of GitLab QA at FOSDEM 2018](https://youtu.be/arPxzixIl38).

----

[Back to README.md](../README.md)
