# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

require 'gitlab/qa'

require 'climate_control'
require 'webmock/rspec'
require 'timecop'

RSpec.configure do |config|
  config.filter_run_when_matching :focus

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.disable_monkey_patching!
  config.expose_dsl_globally = true
  config.warnings = true
  config.profile_examples = 10
  config.order = :random
  Kernel.srand config.seed
end
