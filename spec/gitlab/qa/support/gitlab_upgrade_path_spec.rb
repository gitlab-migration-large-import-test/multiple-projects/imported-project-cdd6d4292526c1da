# frozen_string_literal: true

require "logger"

describe Gitlab::QA::Support::GitlabUpgradePath do
  subject(:upgrade_path) { described_class.new(current_version, semver, "ee").fetch }

  let(:current_version) { "15.3.0-pre" }

  let(:tags) do
    <<~JSON
      [
        {"layer": "", "name": "latest"},
        {"layer": "", "name": "14.8.0-ee.0"},
        {"layer": "", "name": "14.9.5-ee.0"},
        {"layer": "", "name": "14.10.5-ee.0"},
        {"layer": "", "name": "15.0.5-ee.0"},
        {"layer": "", "name": "15.2.1-ee.0"}
      ]
    JSON
  end

  let(:upgrade_path_yml) do
    <<~YML
      - major: 13
        minor: 8
      - major: 13
        minor: 12
      - major: 14
        minor: 0
        comments: "**Migrations can take a long time!**"
      - major: 14
        minor: 3
      - major: 14
        minor: 9
      - major: 14
        minor: 10
      - major: 15
        minor: 0
    YML
  end

  before do
    allow(Gitlab::QA::Runtime::Logger).to receive(:logger) { Logger.new(StringIO.new) }

    stub_request(:get, "https://registry.hub.docker.com/v2/namespaces/gitlab/repositories/gitlab-ee/tags?page=1&page_size=100")
      .with(body: "{}")
      .to_return(status: 200, body: %({ "results": #{tags}, "next": null }))

    stub_request(:get, "https://gitlab.com/gitlab-com/support/toolbox/upgrade-path/-/raw/main/upgrade-path.yml")
      .with(body: {})
      .to_return(status: 200, body: upgrade_path_yml)
  end

  context "with upgrade from previous major" do
    let(:semver) { "major" }

    it "returns upgrade path between major versions" do
      expect(upgrade_path.map(&:to_s)).to eq(["gitlab/gitlab-ee:14.10.5-ee.0", "gitlab/gitlab-ee:15.0.5-ee.0"])
    end
  end

  context "with upgrade from previous minor" do
    let(:semver) { "minor" }

    it "returns upgrade path between minor versions" do
      expect(upgrade_path.map(&:to_s)).to eq(["gitlab/gitlab-ee:15.2.1-ee.0"])
    end
  end
end
